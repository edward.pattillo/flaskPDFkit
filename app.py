# import necessary modules
from flask import Flask, render_template, make_response
import pdfkit
import sqlite3


# create the flask object
app = Flask(__name__)

# set PDF details, this uses a Windows executable
# for Linux or OSX you probably need to change this
# visit https://wkhtmltopdf.org/ for the appropriate package
path_wkthmltopdf = 'venv\\Include\\wkhtmltopdf.exe'
app.conf = pdfkit.configuration(wkhtmltopdf=path_wkthmltopdf)
app.css = 'static/style.css'

# ---------------------------------------------------------

# this is the endpoint when no arguments are passed in the URL
@app.route('/')
def home():

    return '<h1>Home</h1>'


# ---------------------------------------------------------


# this endpoint will accept some URL arguments and use them in the output PDF
@app.route('/<name>/<location>')
def pdf_template(name, location):
     
    # this is how to create a PDF from any web page
    # we're not using it here but I think it's cool so I'm keeping it.
    # pdf = pdfkit.from_url("https://digitaldojo.nz/", "dojo.pdf", configuration=config)

    rendered = render_template('cards.html', name=name, location=location)
    pdf = pdfkit.from_string(rendered, False, css=app.css, configuration=app.conf)

    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'inline; filename=output.pdf'

    return response


# ---------------------------------------------------------


# this describes what happens at the /countries endpoint
@app.route('/countries')
def countries():
 
    # open and close a database connection
    with sqlite3.connect("database\\countryDatabase.db") as con:

        # Makes the result a dictionary
        # dictionaries are very easy to work with as we can access
        # the column names directly. eg: result['username']
        con.row_factory = sqlite3.Row
        # create database cursor
        cur = con.cursor()
        # execute SQL that selects the country details
        cur.execute("SELECT * FROM countries_by_population WHERE population > ?",[100000000])
        # assigns all of the results to the 'result' variable
        result = cur.fetchall()

        # If we actually found a result
        if result:
            
            # this calls the Jinja2 template and creates the HTML
            # which is passed back as a string to 'rendered' variable
            rendered = render_template('countries.html',countries=result)

            # at this point we could just return the rendered HTML
            # return rendered

            # but we're going to convert the HTML to a PDF file
            pdf = pdfkit.from_string(rendered, False, css=app.css, configuration=app.conf)

            # header info so that the browser knows we want it to display a PDF
            response = make_response(pdf)
            response.headers['Content-Type'] = 'application/pdf'
            response.headers['Content-Disposition'] = 'inline; filename=output.pdf'

            # present the PDF in the browser
            return response
  

# ---------------------------------------------------------


# the code under this script will only run if this file is run directly
# it won't run if it is imported by another file
if __name__ == '__main__':

    # run the dev server in debug mode
    app.run(debug=True)